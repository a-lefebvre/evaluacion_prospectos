const Table = {
    cat_contacto: 'cat_contacto',
    cat_direccion: 'cat_direccion',
    cat_prospecto: 'cat_prospecto',
    documentacion: 'documentacion'
}

module.exports = {
    Table,
}