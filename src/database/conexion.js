const mysql = require('mysql2');
const { decrypt } = require('../encrypt/AES');

const config = {
    host: decrypt(process.env.DB_HOST),
    user: decrypt(process.env.DB_USER),
    password: decrypt(process.env.DB_PASS),
    database: decrypt(process.env.DB_NAME)
        // host: process.env.DB_HOST,
        // user: process.env.DB_USER,
        // password: process.env.DB_PASS,
        // database: process.env.DB_NAME
};

const pool = mysql.createPool(config);

module.exports = pool;