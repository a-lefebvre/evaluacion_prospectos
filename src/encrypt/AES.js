const crypto = require('crypto-js')


const secret_word = process.env.SECRET_WORD;

const encrypt = (cadena) => {
    return crypto.AES.encrypt(cadena, secret_word).toString();
}

const decrypt = (cadena) => {
    let bytes = crypto.AES.decrypt(cadena, secret_word);
    return bytes.toString(crypto.enc.Utf8);
}

module.exports = {
    encrypt,
    decrypt
}