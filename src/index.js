const express = require('express');
const cors = require('cors');
require('dotenv').config();

//=====INIT
const server = express();

//===MIDDLEWARES
server.use(cors());

//===CONFIG
server.use(express.json({ limit: '50mb' }));

//====ROUTES
server.use('/api', require('./routes/prospecto.routes'));
server.use('/api', require('./routes/contacto.routes'));
server.use('/api', require('./routes/direccion.routes'));
server.use('/api', require('./routes/documentos.routes'));

//====INIT SERVER
server.listen(process.env.PORT, () => {
    console.log('Servidor corriendo en el puerto ' + process.env.PORT);
});