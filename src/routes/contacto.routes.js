const { Router } = require('express');
const { check } = require('express-validator');
const { contactoCtrl } = require('../controllers/contacto.controller');
const { validarCampos } = require("../middlewares/validar_campos");
const router = Router();


router.post('/telefono', [
    check('fk_cat_prospecto', 'El fk_cat_prospecto del Prospecto es Obligatorio').not().isEmpty(),
    check('telefono', 'El telefono del Prospecto es Obligatorio').not().isEmpty(),
    validarCampos
], contactoCtrl.postContacto);


module.exports = router;