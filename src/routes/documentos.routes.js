const { Router } = require('express');
const { check } = require('express-validator');
const { documentoCtrl } = require('../controllers/documentos.controller');
const { validarCampos } = require("../middlewares/validar_campos");
const router = Router();

router.get('/documentos/:pk_cat_prospecto', documentoCtrl.getDocumento);
router.post('/documentos', [
    check('fk_cat_prospecto', 'El fk_cat_prospecto del Prospecto es Obligatorio').not().isEmpty(),
    check('documentos', 'Los Documentos del Prospecto son Obligatorios').not().isEmpty(),
    validarCampos
], documentoCtrl.postDocumento);


module.exports = router;