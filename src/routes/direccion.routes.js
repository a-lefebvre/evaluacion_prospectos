const { Router } = require('express');
const { check } = require('express-validator');
const { direccionCtrl } = require('../controllers/direccion.controller');
const { validarCampos } = require("../middlewares/validar_campos");
const router = Router();


router.post('/direccion', [
    check('calle', 'La calle de la direccion es Obligatorio').not().isEmpty(),
    check('numero', 'El numero de la direccion es Obligatorio').not().isEmpty(),
    check('colonia', 'La colonia de la direccion es Obligatorio').not().isEmpty(),
    check('cp', 'El codigo postal de la direccion es Obligatorio').not().isEmpty(),
    check('fk_cat_prospecto', 'El fk_cat_prospecto es Obligatorio').not().isEmpty(),
    validarCampos
], direccionCtrl.postDireccion);


module.exports = router;