const { Router } = require('express');
const { check } = require('express-validator');
const { prospectoCtrl } = require('../controllers/prospecto.controller');
const { validarCampos } = require("../middlewares/validar_campos");
const router = Router();

router.get('/prospectos', prospectoCtrl.getAllProspectos);
router.get('/prospectos/:pk_prospecto', prospectoCtrl.getProspecto);
router.post('/prospectos', [
    check('nombre', 'El Nombre del Prospecto es Obligatorio').not().isEmpty(),
    check('primer_apellido', 'El Primer Apellido del Prospecto es Obligatorio').not().isEmpty(),
    check('rfc', 'El RFC del Prospecto es Obligatorio').not().isEmpty(),
    validarCampos
], prospectoCtrl.postProspecto);
router.put('/prospectos/:pk_prospecto', [
    check('estatus', 'El Nuevo Estatus es Obligatorio').not().isEmpty(),
    check('observaciones', 'Las Observaciones Son Necesarias').not().isEmpty(),
    validarCampos
], prospectoCtrl.updateProspecto);

module.exports = router;