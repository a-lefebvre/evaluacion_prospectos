const { table } = require('console');
const pool = require('../database/conexion');
const { Table } = require('../database/tablas');

const documentoModel = {};

documentoModel.getdocumentos = (pk_prospecto, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let consulta = `SELECT * FROM ${Table.documentacion} 
                        WHERE fk_cat_prospecto = ${pk_prospecto}`;
        conexion.query(consulta, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta' + error, null);
            } else {
                if (results.length > 0) {
                    callback(null, results);
                } else {
                    callback('No se encontraron datos', null);
                }
            }
        });
        conexion.release();
    });
}

documentoModel.postDocumentos = (registro, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let params = getParametros(registro);
        let consulta = `INSERT INTO ${Table.documentacion} 
                        (documento, fk_cat_prospecto) 
                        VALUES (?, ?)`;

        conexion.query(consulta, params, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta', null);
            } else {
                if (results.affectedRows > 0) {
                    callback(null, results);
                } else {
                    callback('No Se Guardaron Datos', null);
                }
            }
        });
        conexion.release();
    });
}
const getParametros = (objeto) => {
    let array = [];
    for (const key in objeto) {
        array.push(objeto[key]);
    }
    return array;
}

module.exports = {
    documentoModel
}