const pool = require('../database/conexion');
const { Table } = require('../database/tablas');

let prospectoModel = {};

prospectoModel.getAllProspectos = (callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let consulta = `SELECT CP.*, IFNULL(CD.calle, '') calle, IFNULL(CD.numero, '') numero, 
                        IFNULL(CD.colonia, '') colonia, IFNULL(CD.cp, '') cp, IFNULL(CC.telefono, '') telefono
                        FROM ${Table.cat_prospecto} CP
                        LEFT JOIN ${Table.cat_direccion} CD ON CD.fk_cat_prospecto = CP.pk_cat_prospecto AND CD.estatus != 'B' 
                        LEFT JOIN ${Table.cat_contacto} CC ON CC.fk_cat_prospecto = CP.pk_cat_prospecto AND CC.estatus != 'B'
                        WHERE CP.estatus != 'B' ORDER BY pk_cat_prospecto ASC`;
        conexion.query(consulta, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta' + error, null);
            } else {
                if (results.length > 0) {
                    callback(null, results);
                } else {
                    callback('No se encontraron datos', null);
                }
            }
        });
        conexion.release();
    });
}
prospectoModel.getProspecto = (pk_prospecto, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let consulta = `SELECT CP.*, IFNULL(CD.calle, '') calle, IFNULL(CD.numero, '') numero, 
                        IFNULL(CD.colonia, '') colonia, IFNULL(CD.cp, '') cp, IFNULL(CC.telefono, '') telefono
                        FROM ${Table.cat_prospecto} CP
                        LEFT JOIN ${Table.cat_direccion} CD ON CD.fk_cat_prospecto = CP.pk_cat_prospecto AND CD.estatus != 'B' 
                        LEFT JOIN ${Table.cat_contacto} CC ON CC.fk_cat_prospecto = CP.pk_cat_prospecto AND CC.estatus != 'B'
                        WHERE CP.pk_cat_prospecto = ${pk_prospecto} AND CP.estatus != 'B'`;

        conexion.query(consulta, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta', null);
            } else {
                if (results.length > 0) {
                    callback(null, results);
                } else {
                    callback('No se encontraron datos', null);
                }
            }
        });
        conexion.release();
    });
}
prospectoModel.postProspecto = (prospecto, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let params = getParametros(prospecto);
        let consulta = `INSERT INTO ${Table.cat_prospecto} 
                        (nombre, primer_apellido, segundo_apellido, rfc) 
                        VALUES (?, ?, ?, ?)`;

        conexion.query(consulta, params, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta', null);
            } else {
                if (results.affectedRows > 0) {
                    const insertId = results.insertId;

                    callback(null, results);
                } else {
                    callback('No Se Guardaron Datos', null);
                }
            }
        });
        conexion.release();
    });
}
prospectoModel.updateProspecto = (pk_prospecto, evaluacion, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let estatus = evaluacion.estatus;
        let observaciones = evaluacion.observaciones;
        let consulta = `UPDATE ${Table.cat_prospecto}
                        SET estatus =  '${estatus}',
                            observaciones = '${observaciones}',
                            fecha_modificacion = now()
                        WHERE pk_cat_prospecto =  ${pk_prospecto}`;

        conexion.query(consulta, (error, results) => {
            if (error) {
                callback('Error Al Hacer La Consulta', null);
            } else {
                if (results.affectedRows > 0) {
                    callback(null, results);
                } else {
                    callback('No Se Guardaron Datos', null);
                }
            }
        });
        conexion.release();
    });
}

const getParametros = (objeto) => {
    let array = [];
    for (const key in objeto) {
        array.push(objeto[key]);
    }
    return array;
}

module.exports = {
    prospectoModel
}