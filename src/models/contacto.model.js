const pool = require('../database/conexion');
const { Table } = require('../database/tablas');

const contactoModel = {};

contactoModel.postTelefono = (telefono, callback) => {
    pool.getConnection((err, conexion) => {
        if (err) {
            callback('No Se Pudo Establecer Conexion', null);
        }
        let consulta = `INSERT INTO ${Table.cat_contacto} 
                        (telefono, fk_cat_prospecto) 
                        VALUES (?, ?)`;
        let params = getParametros(telefono);
        conexion.query(consulta, params, (error, results) => {
            if (error) {
                callback('Error Al Ejecutar La Consulta', null);
            } else {
                if (results.affectedRows > 0) {
                    callback(null, results);
                } else {
                    callback('No Se Guardaron Datos', null);
                }
            }
        });
        conexion.release();
    });
}

const getParametros = (objeto) => {
    let array = [];
    for (const key in objeto) {
        array.push(objeto[key]);
    }
    return array;
}

module.exports = {
    contactoModel
}