const { direccionModel } = require('../models/direccion.model');

const direccionCtrl = {};

direccionCtrl.postDireccion = (req, res) => {
    const direccion = req.body;
    direccionModel.postDireccion(direccion, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}

module.exports = {
    direccionCtrl
}