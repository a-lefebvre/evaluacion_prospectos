const { prospectoModel } = require('../models/prospecto.model');

const prospectoCtrl = {};

prospectoCtrl.getAllProspectos = (req, res) => {
    prospectoModel.getAllProspectos((err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}
prospectoCtrl.getProspecto = (req, res) => {
    const pk_prospecto = req.params.pk_prospecto;
    prospectoModel.getProspecto(pk_prospecto, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}
prospectoCtrl.postProspecto = (req, res) => {
    const prospecto = req.body;
    prospectoModel.postProspecto(prospecto, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}
prospectoCtrl.updateProspecto = (req, res) => {
    const pk_prospecto = req.params.pk_prospecto;
    const evaluacion = req.body;
    prospectoModel.updateProspecto(pk_prospecto, evaluacion, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}

module.exports = {
    prospectoCtrl
}