const { contactoModel } = require('../models/contacto.model');

const contactoCtrl = {};

contactoCtrl.postContacto = (req, res) => {
    const telefono = req.body;
    contactoModel.postTelefono(telefono, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}

module.exports = {
    contactoCtrl
}