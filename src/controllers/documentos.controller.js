const { documentoModel } = require('../models/documentos.model');

const documentoCtrl = {};

documentoCtrl.getDocumento = (req, res) => {
    const pk_cat_prospecto = req.params.pk_cat_prospecto;
    documentoModel.getdocumentos(pk_cat_prospecto, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}
documentoCtrl.postDocumento = (req, res) => {
    const row = req.body;
    let documentos = row.documentos;
    let registro = {
        documentos: JSON.stringify(documentos),
        fk_cat_prospecto: row.fk_cat_prospecto
    }
    documentoModel.postDocumentos(registro, (err, data) => {
        if (err) {
            return res.status(400).json({
                estatus: 400,
                errors: err

            });
        }
        return res.json({
            status: 200,
            data
        });
    })
}

module.exports = {
    documentoCtrl
}